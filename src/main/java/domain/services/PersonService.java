package domain.services;

import domain.*;

import java.util.ArrayList;
import java.util.List;

public class PersonService {

	private static List<Person> db = new ArrayList<Person>();
	private static int currentID =1;
	
	public Person get(int id){
		
		for(Person p: db){
			if (p.getId()==id){
				return p;
			}
		}
		return null;
		
	}
	
	public void add(Person person){
		
		person.setId(++currentID);
		db.add(person);
		
	}
	
	public void update (Person person){
		
		for (Person p: db){
			if (p.getId()==person.getId()){
				p.setName(person.getName());
				p.setSurname(person.getSurname());
			}
		}
	}
	
	public void delete (Person p){
		
		db.remove(p);
	}

	public List<Person> getAll() {
		
		return db;
	}
	
}
